import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Collections;

/**
 * Crie uma solução na linguagem de sua preferência que:
  - Receba uma lista com nomes: ["Marcos", "Maria", "Carlos", "José", "Aline", "Júlia", "Júlio", "Renato", "Maria", "Thiago"]
  - Ordene por ordem alfabética.
  - Retire os nomes duplicados.
  - Imprima o resultado.
 */
public class Main
{
    public static void main (String[] args){
        
        Scanner le = new Scanner(System.in);
        
        ArrayList<String> nomes= new ArrayList<>();
        nomes.add("Marcos");
        nomes.add("Maria");
        nomes.add("Carlos"); 
        nomes.add("José");
        nomes.add("Aline"); 
        nomes.add("Júlia"); 
        nomes.add("Júlio");  
        nomes.add("Renato"); 
        nomes.add("Maria"); 
        nomes.add("Thiago");
        
        System.out.println("-- Lista Original --");

        for(int i=0; i<nomes.size();i++){
            System.out.println(nomes.get(i));
        }
        
        
        System.out.println("-- Lista Ordenada --");
        
        ArrayList<String> listaOrdenada= ordenarLista(nomes);
        
        for(int i=0; i<listaOrdenada.size();i++){
            System.out.println(listaOrdenada.get(i));
        }
        
        
        /* Fazer um for percorrendo a lista...pega o objeto e compara, se igual chamar o metodo delete*/
        System.out.println("-- Lista sem nome duplicado --");
        ArrayList<String> listaSemRepeticao= tirarDuplicado(listaOrdenada);
        
      
        for(int i=0; i<listaSemRepeticao.size();i++){
            System.out.println(listaSemRepeticao.get(i));
        }
    }
    
    static ArrayList<String> ordenarLista(ArrayList<String> nomes){
        Collections.sort(nomes);

        return nomes;
    }
    
    static ArrayList<String> tirarDuplicado(ArrayList<String> nomes){
        
        for(int i=0; i<nomes.size()-1;i++){
            
            String a= new String();
            a=nomes.get(i);
            String b= new String();
            b=nomes.get(i+1);
            
            if(a.equalsIgnoreCase(b)){
                nomes.remove(nomes.get(i));
            }
            
        }
        
        return nomes;
    }
}
